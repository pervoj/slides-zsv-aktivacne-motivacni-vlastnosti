---
marp: true
footer: "&copy; 2023 Dominik Bartuška, Vojtěch Perník"
style: |
    @import url("https://rsms.me/inter/inter.css");

    section {
        justify-content: flex-start;
        font-family: "Inter", sans-serif;
    }

    h1 {
        color: inherit;
        font-weight: 500;
        margin-block-end: 0.4em;
    }

    h1::after {
        content: "";
        display: block;
        height: 0.1em;
        width: 1.6em;
        margin-block-start: 0.08em;
        background: #1c71d8;
    }

    a {
        color: #1c71d8;
        text-decoration: underline;
    }

    section.main-title {
        justify-content: center;
        align-items: center;
        text-align: center;
    }

    section.section-title {
        justify-content: center;
    }

    section.main-title h1,
    section.section-title h1 {
        font-size: 2em;
        margin-block-end: 0.2em;
    }

    section.main-title h1::after,
    section.section-title h1::after {
        margin-block-start: 0.4em;
    }

    section.main-title h1::after {
        margin-inline: auto;
    }

    section.image {
        padding: 0;
        flex-direction: row;
        align-items: flex-end;
    }

    section.image > * {
        flex: 1;
    }

    section.image footer {
        display: none;
    }

    section.image h1 {
        color: white;
        background-color: #00000099;
        line-height: 1;
        margin: 0;
        padding: 0.4em 0.6em;
        font-size: 0.8em;
        font-weight: 400;
    }

    section.image h1::after {
        display: none;
    }
---

<!--
_class: main-title
_footer: "CC BY-SA 4.0"
-->

# Aktivačně-motivační vlastnosti

Dominik Bartuška
Vojtěch Perník

---

![bg](./images/diagram-motivace.drawio.svg)

<!--
MOTIVACE - to, co nás pohání k určitému chování

MOTIV - je jejím bezprostředním spouštěčem

MOTIVAČNÍ ČINITELÉ - skládá se z nich výsledný motiv
-->

---

<!--
_class: section-title
-->

# Motivace

---

# Motivace

* proces, který **pohání** jedince **k určitému chování**
* slovo pochází z latinského **„motus“** (pohyb)
* vyvolaná **motivem**, končí **realizací**

<!--
Je tím samotným motorem, který spouští určité chování.

motus - pohyb - hybná síla chování

Motivaci vždy vyvolá motiv a ukončena je realizací motivu (jeho uspokojením).
-->

---

# Motiv

* **podnicuje** člověka **k aktivitě** a udržuje ho v ní
<br>
* **impulzy** – vnitřní motivy (potřeba spánku, uspokojení hladu)
* **incentivy** – vyvolané okolním prostředím
<br>
* **vyšší** a **nižší** motivy *(Maslowova pyramida)*

<!--
Je podnět ke spuštění a bezprostředí spouštěč motivace.

(např. krimi - motiv činu)

Dělení - podle původu motivu
======
- vnitřní - IMPULZY
- vnější - INCENTIVY

Dělení - řád - VYŠŠÍ a NIŽŠÍ
======
viz Maslowova pyramida - další slide
-->

---

![bg](./images/maslowova-pyramida.jpg)

---

<!--
_class: section-title
-->

# Potřeby a pudy

---

# Potřeby

* biologická nebo psychická **nerovnováha** (nedostatek, nadbytek)
* působí aktivačně, dokud není uspokojena
<br>
* **biologické** – nutné k přežití jedince a rodu
* **sociální** – spjaté se sociálním prostředím
  * **psychické** – vycházející ze vztahů mezi lidmi
  * **kulturní** – ve vztahu člověka ke kultuře

<!--
Vznikají NEROVNOVÁHOU v organizmu (biologická/psychická) a působí na nás dokud ji znovu NEVYROVNÁME.

Dělení
======
1. biologické - souvisí s přežitím jedince nebo i rodu
      - potrava, světlo, teplo, …
2. sociální - všechny ostatní, vychází ze sociálního prostředí
      - JEN U LIDÍ
    a. psychické - sociální myšlení, mezilidské vztahy
          - SOUČINNOST a SOUTĚŽ
          - pomoc (přijímání i poskytování)
          - společenská prestiž (důstojnost, úspěch, …)
    b. kulturní - vznik ze vztahu člověka k lokální kultuře
          - uspokojování kulturních potřeb
          - poslouchat hudbu, číst knihu, zprávy, …
-->

---

# Pudy (instinkt)

* vrozená hnací síla k činosti
* možné je **ovládat rozumem**
* intenzita pudů určuje charakteristiku osobnosti
<br>
* vztah pudů a potřeb není jasně vyřešen
* často ztotožňovány s biologickými potřebami

<!--
Vrozená hnací síla, která se NEDÁ NAUČIT, ale je možné je OVLÁDAT ROZUMEM.

Podle jejich intenzity se dá charakterizovat osobnost člověka.

Např: + projevy
=====
pud sebezáchovy - úsilí vyhnout se nebezpečným vlivům
pud hladu a bolesti - úsilí uniknout jim
mateřský pud - péče, pomoc, starostlivost

Není jasně vyřešen rozdíl pudů a biologických potřeb. Někdy i ztotožňovány.
-->

---

<!--
_class: section-title
-->

# Zájmy, záliby a sklony

---

# Zájmy

* trvalé **usilí zabývat se tím, co nás přitahuje** po stránce poznávací nebo citové
* předmětu zájmu je připisován **subjektivně vyšší význam**
<br>
* projevují se:
  * úsilím o **časté střetávání se** s předmětem zájmu
  * úsilím o **lepší poznání** předmětu zájmu
  * **kladným vztahem** k činnosti spojené s předmětem zájmu

<!--
Trvalý vztah k určité činnosti či předmětu, které nás upoutávají.

Člověk zájmům subjektivně připisuje větší význam než ostatním věcem.

[projevy]
-->

---

# Zájmy

* druhy zájmů:
  * **materiální** – uspokojení materiálních potřeb
  * **společenské** – aktivní společenská činnost
  * **duchovní** – touha po poznání a aktivní umělecká činnost

* vlastnosti zájmů:
  * **šířka** (bohatost) – jak rozmanitý a pestrý daný zájem je
  * **hloubka** (síla) – jaké úsilí mu člověk věnuje

* **záliba** – vyhraněný ustřední zájem

---

# Sklony

* **zaměření člověka** na určitou činnost
* **vyvynuté ze zájmů**

<!--
Jde o zaměření člověka na specifickou činnost. Vyvíjí se ze zájmů, ale nejsou s nimi totožné (zájem sledování fotbalu se nemusí pojit se sklonem hrát ho).
-->

---

<!--
_class: section-title
-->

# Aspirace, cíle a životní plány

---

# Aspirace (ambice)

* **zaměření** na určité hodnoty
* projev **snahy o dosažení cílů**
<br>
* **aspirační úroveň** – jak vysoké cíle si jedinec vytyčuje

<!--
Motivace k dosažení určitých vytyčených hodnot a cílů.

Obtížnost vytyčených cílů je úměrná ASPIRAČNÍ ÚROVNI.
-->

---

# Cíle

* uvědomovaný **směr aktivity**, to, **čeho chceme dosáhnout**
<br>
* **krátkodobé** – většinou snadno dosažitelné
* **dlouhodobé** – vyžadují větší úsilí a delší čas
<br>
* charakteristiku osobnosti určuje množství cílů a věnované úsilí

<!--
Stanovený směr, kterým se chceme ubírat. To, čeho chceme v budoucnosti dosáhnout.

Dělení: - podle složitosti
=======
- krátkodobé - jednodušší a rychlejší
- dlouhodobé - složitější a časově náročnější (potřeba rozdělit na menší cíle)

Podle množství a složitosti vytyčených cílu se určuje charakteristika osobnosti.
-->

---

# Životní plány

* systém dlouhodobých cílů, kterých chceme v budoucnosti dosáhnout
<br>
* v dětství se často mění

<!--
Soubor cílů, kterých chceme dosáhnout ve vzdálenější budoucnosti. Určují, jakým směrem se náš život bude vyvíjet.

V dětství se často mění, ale v adolescenci se ustalují.
-->

---

<!--
_class: section-title
-->

# Zvyky

---

# Zvyky

* neuvědomované **zafixované způsoby** chování
<br>
* zakořeněné zvyky se stávájí vlastností osobnosti

<!--
Neuvědomované aktivační činitele, které vznikají naučeným a zažitým způsobem chování.

Dobře zakořeněné zvyky jako naučené a zafixované způsoby chování jsou svým obsahem a tendencí uplatnit se natolik příznačné pro danou osobnost ->

… Jsou-li dobře upevněné, stávají se vlastnostmi osobnosti.
-->

---

<!--
_class: main-title
-->

# Děkujeme za pozornost!

---

<!--
footer: ""
-->

# Zdroje

- http://www.ucenischalupou2.chytrak.cz/psychologie/11.htm

- https://spolecenske-vedy.blogspot.com/2011/01/8-aktivacne-motivacni-vlastnosti.html

- https://www.studium-psychologie.cz/obecna-psychologie/12-motivace-deleni-motivu.html

- https://cs.wikipedia.org/wiki/Motivace

- https://publi.cz/books/171/04.html

- https://turbo.cdv.tul.cz/mod/book/view.php?id=5965&chapterid=6244

---

# Zdroje

- https://cs.wikipedia.org/wiki/Zájmy

- https://cs.wikipedia.org/wiki/Instinkt

- https://encyklopedie.soc.cas.cz/w/Aspirace
